# ExpressJS + SQLite3 CRUD

Ejemplo de CRUD utilizando ExpressJS y SQLite3

## Scripts

### Start

`pnpm start`

Ejecuta el proyecto en modo de desarrolo.

### Dev

`pnpm dev`

Ejecuta el proyecto en modo desarrollo con hot reload, usando nodemon

### Build 

`pnpm build`

Compila los archivos typescript a javascript

### Serve

`pnpm serve`

Ejecuta el proyecto con el proyecto compilado si es que existe

### Preview

`pnpm preview`

Compila el proyecto a javascript y luego lo ejecuta