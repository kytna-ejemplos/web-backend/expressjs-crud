import 'dotenv/config';
import app from './app';

app.listen(app.get('PORT'), () => {
    console.log(`App Listen On Port ${app.get('PORT')}`)
})