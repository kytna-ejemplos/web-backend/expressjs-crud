import { Router } from 'express';
import { GetDB } from '../db';

const router = Router();
const db = GetDB();

router.get('/posts', (req, res) => {
  db.all('SELECT * FROM posts', (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server Error');
      return;
    }

    res.json(data);
  })
})

router.post('/posts', (req, res) => {
  console.log(req.body)

  const { title, content } = req.body;

  if (!title || !content) {
    res.status(400).send(`${!title ? 'Title' : 'Content'} Not Provided`);
    return;
  }

  db.run('INSERT INTO posts (title, content) VALUES($title, $content)', {
    $title: title,
    $content: content
  }, (data, err) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server Error');
      return;
    }
    if (process.env.VERBOSE)
      console.log(data);

    res.json(data);
  });
})

router.put('/posts/:id', (req, res) => {
  const { title, content } = req.body;
  const id = req.params['id'];

  if (!title || !content) {
    res.status(400).send(`${title ? 'Title' : 'Content'} Not Provided`);
    return;
  }

  const query = `UPDATE posts SET ${title ? 'title = $title,' : ''} ${content ? 'content = $content' : ''} WHERE id = $id`;
  console.log(query)
  console.log(id)

  db.run(query, {
    $title: title,
    $content: content,
    $id: id
  }, (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server Error');
      return;
    }

    if (process.env.VERBOSE)
      console.log(data);

    res.json(data);
  });
});

router.get('/posts/:id', (req, res) => {
  const id = req.params['id'];
  console.log(id);

  db.get('SELECT * FROM posts WHERE id = $id', { $id: id }, (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server Error');
      return
    }

    if (process.env.VERBOSE)
      console.log(data);

    res.send(data);
  })
})

router.delete('/posts/:id', (req, res) => {
  const id = req.params['id'];

  db.run('DELETE FROM posts WHERE id = $id', { $id: id }, (data, err) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server Error');
      return
    }

    if (process.env.VERBOSE)
      console.log(data);

    res.json(data);
  })
})

export default router;