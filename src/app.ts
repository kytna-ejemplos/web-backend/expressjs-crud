import express from 'express';
import bp from 'body-parser';
import PostsRoutes from './routes/posts';

const app = express();

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));
app.use(PostsRoutes);
app.use(express.static('public'));

app.set('PORT', process.env.PORT);

export default app;