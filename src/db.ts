import { Database, verbose } from 'sqlite3';

//const db = new Database(process.env.DB_FILENAME, (err) => console.error(err))

const CreatePostsTable = `CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR(32), content TEXT);`;

let db: Database;

export function GetDB(): Database {
  if (db)
    return db;

  if (process.env.VERBOSE)
    verbose();

  db = new Database(process.env.DB_FILENAME, err => console.error(err));

  db.run(CreatePostsTable, (res, err) => {
    if (err)
      throw err;

    if (process.env.VERBOSE)
      console.log(res);
  });

  return db;
}